
This page provides documentation for the command line tools.


## CLI commands

::: mkdocs-click
    :module: flask_mkdocs.documentation.cli
    :command: cli
    :prog_name: flask_mkdocs
    :style: table

<!--|cog p(f'::: {values.project.module}.tools.init_app ') |-->
::: flask_mkdocs.documentation.init_app 
<!--|end|-->