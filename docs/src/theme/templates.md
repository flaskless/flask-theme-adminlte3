## Theme Templates

Use override the templates in this theme:

* Copy the template file to the app's `TEMPLATE_PATH`

or

* Change the name of the template in the instance settings file


??? note "Top Bar - the top navigation bar `includes/navigation.html`"
        
    ```jinja2
    {{code_from_file('theme/templates/includes/navigation.html') | indent(4)}}
    ```

::: flask_theme_adminlte3.theme.constants
    selection:
        members:
            - THEME_SIDEBAR_TEMPLATE  

??? note "Side Bar - the left side navigation bar  `includes/sidebar.html`"
        
    ```jinja2
    {{code_from_file('theme/templates/includes/sidebar.html') | indent(4)}}
    ```

::: flask_theme_adminlte3.theme.constants
    selection:
        members:
            - THEME_CONTROLBAR_TEMPLATE

??? note "Control Bar - the right side 'control' bar `includes/controlbar.html`"
        
    ```jinja2
    {{code_from_file('theme/templates/includes/controlbar.html') | indent(4)}}
    ```

Plus the templates for stylesheets and javascripts:

::: flask_theme_adminlte3.theme.constants
    selection:
        members:
            - THEME_JAVASCRIPT_TEMPLATE

??? note "JavaScript - appears before end of `body` tag `includes/javascript.html`"
        
    ```jinja2
    {{code_from_file('theme/templates/includes/javascript.html') | indent(4)}}
    ```

::: flask_theme_adminlte3.theme.constants
    selection:
        members:
            - THEME_STYLESHEETS_TEMPLATE

??? note "Stylesheets - appears before end of `head` tag `includes/stylesheets.html`"
        
    ```jinja2
    {{code_from_file('theme/templates/includes/stylesheets.html') | indent(4)}}
    ```

You can also customize the branding by editing the standard templates:


??? note "Logo - appears on top of the sidebar `includes/sidebar/logo.html`"
        
    ```jinja2
    {{code_from_file('theme/templates/includes/sidebar/logo.html') | indent(4)}}
    ```

::: flask_theme_adminlte3.theme.constants
    selection:
        members:
            - THEME_FOOTER_TEMPLATE

??? note "Footer - appears on bottom of the page `includes/footer.html`"
        
    ```html
    {{code_from_file('theme/templates/includes/footer.html') | indent(4)}}
    ```


??? note "Dashboard - appears on the dashboard page `pages/dashboard.html`"
        
    === "Template"

        ```html
        {{code_from_file('theme/templates/pages/dashboard.html') | indent(8)}}
        ```

    === "Image"


        ![Placeholder](https://dummyimage.com/600x400/eee/aaa){ align=left }