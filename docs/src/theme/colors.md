---
template: adminlte3.html
---


# Setting Colors

The theme supports the standard Bootstrap colors.  See the [Bootstrap 4 Documentation](https://getbootstrap.com/docs/4.0/utilities/colors/) for more info

!!! note "Use `ThemeColor.<NAME>` with `bg-` and `text-` in your template files."
    Use `ThemeColor` in HTML by adding it to class attributes:
    ```jinja2
    <div class="bg-{%raw%}{{ThemeColor.INFO}}{%endraw%}">
    ```

For more details, see the [official AdminLTE3 documentation](https://adminlte.io/docs/3.1//layout.html)

## Theme Colors

<div class="row">
  <div class="col-sm-4 col-lg-3 p-3 bg-primary"> PRIMARY (primary) / BLUE (blue)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-secondary"> SECONDARY (secondary)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-success"> SUCCESS (success) / GREEN (green)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-info"> INFO (info) / CYAN (cyan)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-warning"> WARNING (warning) / YELLOW (yellow)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-danger"> DANGER (danger) / RED (red)</div>
</div>
<br/><br/>

# Colors

<div class="row">
  <div class="col-sm-4 col-lg-3 p-3 bg-indigo"> INDIGO (indigo)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-navy"> NAVY (navy)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-purple"> PURPLE (purple)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-fuchsia"> FUCHSIA (fuchsia)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-pink"> PINK (pink)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-maroon"> MAROON (maroon)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-orange"> ORANGE (orange)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-lime"> LIME (lime)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-teal"> TEAL (teal)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-olive"> OLIVE (olive)</div>
</div>
<br/><br/>

# Black/White Nuances

<div class="row">
  <div class="col-sm-4 col-lg-3 p-3 bg-black"> BLACK (black)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-gray-dark"> GRAY Dark (gray-dark)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-gray"> GRAY (gray)</div>
  <div class="col-sm-4 col-lg-3 p-3 bg-light"> LIGHT (light)</div>
</div>
<br/><br/>