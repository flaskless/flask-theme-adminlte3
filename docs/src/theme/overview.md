---
hero: Lorem ipsum dolor sit amet
---

This module provides [Jinja2 templates](https://jinja.palletsprojects.com/) for [AdminLTE v3.1](https://adminlte.io/themes/v3/) to work with [Flask](https://flask.palletsprojects.com/). Templates can be easily extended or overriden to provide new themes and features. Also, this module includes a set of **AdminLTE3** styled templates for authentication (login, register, change password etc.) to replace those provided by the [Flask-Security-Too](https://flask-security-too.readthedocs.io/en/stable/) extension.

![AdminLTE Theme](screenshot1.png)

## Usage:

#### 1. Initialize the with your Flask app `app`

```
theme = Theme()
theme.init_app(app)
```

#### 2. Extend the base template

```jinja2
{% raw %}
{% extends config.BASE_TEMPLATE %}
{% endraw %}
...
```

#### 3. Configure the layout and colors

Update settings in the instance configuration file, usually `instance/settings.py` 

```python
THEME_LAYOUT='layout-topnav'
```

See all possible settings in the [Themes documentation](../settings)


## Features:

*  **AdminLTE3** styled authentication views that you can use in replacement of the ones that are provided by the [Flask-Security-Too](https://flask-security-too.readthedocs.io/en/stable/)
* YAML based navigation menus.
* [Fullscreen](../settings/#saasless.extensions.theme.constants.THEME_FULLSCREEN_SWITCH_ENABLED) and [Darkmode](../settings/#saasless.extensions.theme.constants.THEME_DARKMODE_SWITCH_ENABLED) settings
* Easy to customize and override layouts and sections