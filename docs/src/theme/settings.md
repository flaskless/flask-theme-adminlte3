
## Usage

Use the setting values in code:

* To use `config.<NAME>` in template files
* To use `flask.current_app.config['<NAME>']` in python files

::: flask_theme_adminlte3.theme.constants