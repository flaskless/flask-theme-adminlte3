
## Theme Layouts

Use the setting values in code:

* To use `config.<NAME>` in template files
* To use `flask.current_app.config['<NAME>']` in python files

The main layout is contained in `base.html`:


??? note "See the `base.html` template"
    
    ```jinja2
    {{code_from_file('theme/templates/layouts/base.html','python') | indent(4)}}
    ```

The Layout is separated into 5 sections.  The 3 menu bars for navigation:

::: flask_theme_adminlte3.theme.constants
    rendering:
        show_source: true
        show_if_no_docstring: true
    selection:
        members:
        - THEME_LAYOUT_BOXED
        - THEME_LAYOUT_COLLAPSED_SIDEBAR 
        - THEME_LAYOUT_SIDEBAR_MINI
        - THEME_LAYOUT_FIXED_FOOTER
        - THEME_LAYOUT_FIXED_SIDEBAR
        - THEME_LAYOUT_FIXED_TOP_NAV
        - THEME_LAYOUT_TOP_NAV


