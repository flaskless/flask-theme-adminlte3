## Project Status
Here you can see the status of the project.


<style>
#content{
 min-height: 100vh;
color:#1b1b1b; /*-light black-*/
font-size: 1.1em;
background-color:#f2f0ea;/* --off- yellow-white-- */
padding:1%;
border-radius:5px;
}
</style>

## Architectural Decisions

The current status of all architectural decisions

<div style="content">
   <iframe style="overflow:hidden; display:block;  min-height: 50vh;  background-color: #f2f0ea; border: none;" 
            src="{{config.site_url}}/adr/report.html" 
            height=100%; width=100%></iframe>
</div>

## Test Results

Here are the latest Test Results

<div style="content">
   <iframe style="overflow:hidden; display:block;  min-height: 50vh;  background-color: #f2f0ea; border: none;" 
            src="{{config.site_url}}/tests/report.html" 
            height=600%; width=100%></iframe>
</div>

## Test Code Coverage

See the code coverage report

<div style="content">
   <iframe style="overflow:hidden; display:block;  min-height: 50vh;  background-color: #f2f0ea; border: none;" 
            src="{{config.site_url}}/tests/coverage/index.html" 
            height=600%; width=100%></iframe>
</div>
