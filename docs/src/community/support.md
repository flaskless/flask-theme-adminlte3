---
id: support
title: Support and community
description: Support and Community Details and Links
---

- [Discord chatroom](https://discord.gg/MUpMjP2) - Get support or discuss the
  project.
- [Good First Issues](https://gitlab.com/flaskless/flask-theme-adminlte3/contribute) - Start
  here if you want to contribute.
- [RFCs](https://github.com/flaskless/flask-theme-adminlte3/labels/rfc) - Help shape the
  technical direction by reviewing _Request for Comments_ issues.
- [FAQ](../FAQ.md) - Frequently Asked Questions.
- [Code of Conduct](https://github.com/flaskless/flask-theme-adminlte3/blob/master/CODE_OF_CONDUCT.md) -
  This is how we roll.
- [Blog](https://saasless.io/blog/) - Announcements and updates.
- [Newsletter](https://mailchi.mp/flaskless/flask-theme-adminlte3-community) - Subscribe to
  our email newsletter.
- Give us a star ⭐️ - If you are using Flask Theme AdminLTE or think it is an interesting
  project, we would love a star! ❤️
