

![!](./background/integrations.png){ loading=lazy style="width:50%" align=right }

{% include-markdown "../../README.md" 
   start="<!---|START introduction|--->"
   end="<!---|END introduction|--->"
%}

[readme](vscode://file//Users/alir/Work/flask-theme-adminlte3/README.md)

## Features

  * **AdminLTE3** theme using Bootstrap 4.
  * **Flask-Admin** - quickly create user interfaces (create, read, update, delete) for models.
  * **Flask-Security-Too** - role-based-access-control, two-factor-authentication and much more.
  * **Navigation based on YAML** - no need to write error-prone HTML for navigation.
  * **Serverless** - ready to deploy to AWS Lambda using [Serverless](https://serverless.com).


![./images/saasychefhero.png](./images/saasychefhero-small.png){ loading=lazy align=right }
