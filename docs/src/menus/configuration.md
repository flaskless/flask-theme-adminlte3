# Menu Configuration


## Usage

To add to a menu item:

* Use `add_menu_items` for dynamically loading menu
* Use `yaml_to_menu` to load menu items from a YAML file

## YAML configuration

The menu is contained in `menus.yml `:


??? note "See the `menu/menus.yml` file"
    
    ```jinja2
    {{code_from_file('menu/menus.yml','python') | indent(4)}}
    ```


::: flask_theme_adminlte3.menu.init_app

::: flask_theme_adminlte3.menu.MenuLoader
